﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Casing : MonoBehaviour
{
   [SerializeField]
   private AudioClip _casingSound;
   [SerializeField]
   private AudioSource _audioSource;
   [SerializeField]
   private float _casingLifeSpawn = 3f;
   [SerializeField]
   private float _playSoundDelay = 1f;

   private void Start()
   {
        //Start the spawn timer
        Destroy(gameObject, _casingLifeSpawn);

        //Get a casing sound 
        _audioSource.clip = _casingSound;
        //Play the casing sound 
        StartCoroutine(PlaySound());

        
   }
    private IEnumerator PlaySound()
    {
        //Delay casing sound play
        yield return new WaitForSeconds(_playSoundDelay);
         _audioSource.Play();

    }
   
}
