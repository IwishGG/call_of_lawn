﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseCtroller : MonoBehaviour
{
    //Set Base health point
    [SerializeField]
    private float _baseHealth = 100f;

    void Start()
    {
        //Set our base Health Point to _baseHealth
        GameManager.BaseHealth = _baseHealth;
        //Set a BaseHealth clamp rang from 0 to 100 
        GameManager.BaseHealth = Mathf.Clamp(GameManager.BaseHealth, 0, 100);

    }
}
