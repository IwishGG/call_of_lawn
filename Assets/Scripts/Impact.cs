﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Impact : MonoBehaviour
{
    //How long before the impact is destroyed
    [SerializeField]
    private float _spawnTimer = 2.0f;
    [SerializeField]
    private AudioSource _audioSource;

    [SerializeField]
    private AudioClip _impactSound;

    private void Start()
    {
        // Start the spawn timer
        Destroy(gameObject, _spawnTimer);

        //Get a impact sound 
        _audioSource.clip = _impactSound;
        //Play the impact sound
        _audioSource.Play();
    }



}
