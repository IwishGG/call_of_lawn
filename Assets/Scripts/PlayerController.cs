﻿using System;
using System.Linq;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(CapsuleCollider))]
public class PlayerController : MonoBehaviour
{
    [Header("Movement")]
    //Move speed 
    [SerializeField]
    private float _moveSpeed = 6f;
    //Jump speeds
    [SerializeField]
    private float _jumpSpeed = 10f;
    //Sprint Speed Mult 
    [SerializeField]
    private float _sprintSpeedMult = 1.5f;
    //squat Speed
    [SerializeField]
    private float _squatSpeed = 0.2f;
    //squat Height
    [SerializeField]
    private float _squatHeight = 1.2f;
    //Normal Height
    [SerializeField]
    private float _defaultHeight = 1.8f;
    //Jump key
    [SerializeField]
    private KeyCode _jumpKey = KeyCode.Space;
    //Check ground for jump
    [SerializeField]
    private Transform _groundCheck = null;
    [SerializeField]
    private LayerMask _groundCheckLayout;
    [SerializeField]
    private float _checkDistance = 0.2f;
    [SerializeField]
    private AudioSource _playerAudioSource;

    [SerializeField]
    private AudioClip _walkingSound;
    [SerializeField]
    private AudioClip _runingSound;

    private bool _isGrounded, _jumpPressed, _isRuning, _isSquating;

    // Move value
    private float _xInput, _zInput;
    // Jump value
    private float _yVel;
    private float _speedMult;
    

    private Vector3 _moveVector;
    private Rigidbody _rigidBody;
    private CapsuleCollider _capsuleCol;


    private void Start()
    {
        //Get rigidbody component
        _rigidBody = GetComponent<Rigidbody>();
        //Get AudioSource component
        _playerAudioSource = GetComponent<AudioSource>();
        //Get CapsuleCollider component
        _capsuleCol = GetComponent<CapsuleCollider>();
        //Set walkingSound to Audio Source clip
        _playerAudioSource.clip = _walkingSound;
        //Set Height to default height
        _capsuleCol.height = _defaultHeight;
    }
    private void Update()
    {
        //Read all input
        ReadInput();
        //Check is it grounded 
        GroundCheck();
        //Play sound
        PlaySound();


    }
    private void FixedUpdate()
    {
        //Movement function
        MoveAndJump();
        Squat();


    }
    private void ReadInput()
    {
        //Get Input by uing Axis
        _xInput = Input.GetAxis("Horizontal");
        _zInput = Input.GetAxis("Vertical");
        //Press Jump button 
        _jumpPressed |= Input.GetButtonDown("Jump") && _isGrounded;
        //Press LeftShift to sprint run
        _isRuning = Input.GetKey(KeyCode.LeftShift) ? true : false;
        _speedMult = _isRuning ? _sprintSpeedMult : 1f;
        //Press Left LeftControl to Squat
        _isSquating = Input.GetKey(KeyCode.LeftControl) ? true : false;


    }
    private void GroundCheck()
    {
        // Use checksphere to check layer mask ground ,so we can know is it grounded
        _isGrounded = Physics.CheckSphere(_groundCheck.position, _checkDistance, _groundCheckLayout);
    }
    private void MoveAndJump()
    {

        //Movement Physics
        _moveVector = new Vector3(_xInput, 0, _zInput) * _moveSpeed * _speedMult;
        //Velocity direction is players direction, not global
        _moveVector = transform.TransformDirection(_moveVector);
        //Jump Physics
        _moveVector.y = _jumpPressed && _isGrounded ? _jumpSpeed : _rigidBody.velocity.y;
        _jumpPressed = false;

        //Give rigdbody velocity from moveVector
        _rigidBody.velocity = _moveVector  ;
    }

    private void Squat()
    {
        //Check if squating
        if (_isSquating)
        {
            //Change player cpasule height to squating height 
            _capsuleCol.height = Mathf.Lerp(_capsuleCol.height, _squatHeight, _squatSpeed * Time.deltaTime);
   
        }
        else
        {
            //Change player cpasule height to normal height 
            _capsuleCol.height = Mathf.Lerp(_capsuleCol.height, _defaultHeight, _squatSpeed * Time.deltaTime);  
        }
    }
    private void PlaySound()
    {
        //Check if player is grounded and his sqrMagnitude velocity >0.1f and not runing, play walking sound
        if (_isGrounded && _rigidBody.velocity.sqrMagnitude > 0.1f && _isRuning == false)
        {
            //Sound clip is walking sound
            _playerAudioSource.clip = _walkingSound;
            //Check if no sound is playing
            if (!_playerAudioSource.isPlaying)
            {
                _playerAudioSource.Play();
            }
        }
        //Check if player is grounded and his sqrMagnitude velocity >0.1f and runing, play Runing sound
        else if (_isGrounded && _rigidBody.velocity.sqrMagnitude > 0.1f && _isRuning)
        {
            //Sound clip is runing sound
            _playerAudioSource.clip = _runingSound;
            //Check if no sound is playing
            if (!_playerAudioSource.isPlaying)
            {
                _playerAudioSource.Play();
            }
        }
        else
        {
            if (_playerAudioSource.isPlaying)
            {
                _playerAudioSource.Pause();
            }
        }

    }
   
}
