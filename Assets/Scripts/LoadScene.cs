﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadScene : MonoBehaviour
{

    public void BackToMenu()
    {
        // Back to Menu Scene
        SceneManager.LoadScene(0);
    }
     public void StartGame()
    {
        // Back to Gameplay Scene
        SceneManager.LoadScene(1);
    }
   

}
