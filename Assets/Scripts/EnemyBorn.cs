﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBorn : MonoBehaviour
{
    //Put all Instantiate enemies to this OBJ
    [SerializeField]
    private GameObject _enemyParent;
    //Put all type of enemies to this array
    [SerializeField]
    private GameObject[] _enemyPool;
    //Put all of enemy born point to this array
    [SerializeField]
    private Transform[] _enemyBornPoint;
    //How long time to born a enemy
    [SerializeField]
    private float _bornTimer = 5f;
    //Max ammount enemies in each point 
    [SerializeField]
    private int _enemyNumToBorn = 6;


    //Existing enemies 
    //How many enemies here
    private int _enemyNum;

    private int _enemyTypeNum;


    void Start()
    {
        //delay 1s to increase enemies after start, and every bornTimer once 
        InvokeRepeating("EnemyIncrease", 1, _bornTimer);
    }
    private void EnemyIncrease()

    {
        //Get How many enemies from _enemyParent
        _enemyNum = _enemyParent.transform.childCount;
        //Random to chose a type of enemy  
        int enemyType = Random.Range(_enemyTypeNum, _enemyPool.Length);
        //Random to chose a place of enemt born point 
        int enemyBornPlace = Random.Range(0, _enemyBornPoint.Length);


        //Use score to control instantiate enemy type ,higher score get stronger enemy
        if (GameManager.Score <= 1000)
        {
            _enemyTypeNum = 0;
        }
        else if (GameManager.Score > 1000 && GameManager.Score <= 2500)
        {
            _enemyTypeNum = 1;
        }
        else if(GameManager.Score >= 2500)
        {
            _enemyTypeNum = 2;
        }
        //Check if Existing enemies <= Max enemies amount
        if (_enemyNum <= _enemyNumToBorn)
        {
            //Instantiate a random type of enemy in a random born place           
            GameObject enemy = GameObject.Instantiate(_enemyPool[enemyType], _enemyBornPoint[enemyBornPlace].position, _enemyBornPoint[enemyBornPlace].rotation);
            //Put this enemy transform in _enemyParent this OBJ
            enemy.transform.parent = _enemyParent.transform;

        }




    }
}
