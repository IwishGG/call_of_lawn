﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAI : MonoBehaviour
{
    [SerializeField]
    private GameObject _target;
    [SerializeField]
    private float _enemyMoveSpeed;
    [SerializeField]
    private float _attackDis = 1f;
    [SerializeField]
    private float _damage = 10f;

    [SerializeField]
    private float _attackTimer = 1f;
    private float _timer;


    public static float _distance;
    private Vector3 _enemyPos;





    // Start is called before the first frame update
    void Start()
    {
        transform.LookAt(_target.transform);
        _timer = _attackTimer;
    }

    // Update is called once per frame
    void Update()
    {
        MoveToTarget();
        CheckDistance();

    }

    private void CheckDistance()
    {
        //Get current enemy position 
        _enemyPos = transform.position;
        //Get distance from current enemy to target
        _distance = Vector3.Distance(transform.position, _target.transform.position);
        //Check if the distance <= attack distance
        if (_distance <= _attackDis)
        {
            //enemy stop move
            _enemyMoveSpeed = 0f;
            Attack();

        }

    }

    private void Attack()
    {
        //Set a timer to control How long time enemy attack onces
        _timer -= Time.deltaTime;

        if (_timer <= 0)
        {
            if (GameManager.BaseHealth > 0)
            {
                //take damage 
                GameManager.BaseHealth -= _damage;
                _timer = _attackTimer;
            }
        }
    }

    private void MoveToTarget()

    {
        //Look at target position
        transform.LookAt(_target.transform);
        //Move to target
        transform.Translate(Vector3.forward * Time.deltaTime * _enemyMoveSpeed);
    }


}
