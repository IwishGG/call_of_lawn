﻿using UnityEngine;
using TMPro;
using System.Collections;
using System.Collections.Generic;


public class TextManager : MonoBehaviour
{
    [SerializeField]
    private TMP_Text _scoreText = null;
    [SerializeField]
    private TMP_Text _baseHealthText = null;
    [SerializeField]
    private TMP_Text _ammoText = null;

    [SerializeField]
    private GameObject _gameWinUI;
    [SerializeField]
    private GameObject _gameLoseUI;


    private void Awake()
    {
        //Set Score to 0 when game start
        GameManager.Score = 0;
    }

    private void Update()
    {
        //Show ammo text 
        _ammoText.text = $"AMMO\n{Weapon_Ak.CurrentAmmo}/{Weapon_Ak.Ammo}";
        //Show score text
        _scoreText.text = $"SCORE:{GameManager.Score}";
        //Show lawn health point text
        _baseHealthText.text = $"LAWN\nHP:{GameManager.BaseHealth}";

    }
}
