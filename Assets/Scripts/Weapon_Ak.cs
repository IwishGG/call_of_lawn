﻿using System;
using System.Collections;
using UnityEngine;

public class Weapon_Ak : MonoBehaviour
{
    //Gun camera
    [SerializeField]
    private Camera _gunCamera;
    [Header("Gun")]
    //Bullet speed
    [SerializeField]
    private float _bulletForce = 400f;
    //How long time delay to do reload 
    [SerializeField]
    private float _reloadDelay = 0.6f;
    //How fast the weapon fires, higher value means faster rate of fire
    [SerializeField]
    private float _fireRate = 10f;
    //When Aiming,the gun camera field of view
    [SerializeField]
    private float _scopeFov = 40f;
    //Default camera field of view
    [SerializeField]
    private float _defaultFov = 60f;
    //How fast the camera field of view changes when aiming 
    [SerializeField]
    private float _openScopeSpeed = 15f;
    //How fast casing move
    [SerializeField]
    private float _casingForce = 10f;


    public static int Damage = 10;
    public static int CurrentAmmo = 30;
    public static int Ammo = 30;

    //Used for fire rate
    private float _lastFired;



    [SerializeField]
    private GameObject _bulletPrefab;
    [SerializeField]
    private Transform _bulletSpawnPoint;
    [SerializeField]
    private GameObject _casingPrefab;
    [SerializeField]
    private Transform _casingSpawnPoint;

    [SerializeField]
    private ParticleSystem _muzzleFlash;


    [Header("Audio")]

    [SerializeField]
    private AudioSource _shootAudioSource;
    [SerializeField]
    private AudioSource _mainAudioSource;
    [SerializeField]
    private AudioClip _shootSound;
    [SerializeField]
    private AudioClip _reloadSound;
    [SerializeField]
    private AudioClip _aimSound;


    private bool _isReloading;
    private bool _isAiming;
    private Animator _anim;

    private void Awake()
    {
        //Set current ammo to total ammo value
        CurrentAmmo = Ammo;
        CurrentAmmo = Mathf.Clamp(CurrentAmmo, 0, Ammo);

        //Set the shoot sound to audio source
        _shootAudioSource.clip = _shootSound;

    }
    private void Start()
    {
        _anim = GetComponent<Animator>();
    }


    void Update()
    {
        ReadInput();
        AnimationCheck();
        Aiming();
    }

    private void ReadInput()
    {
        //Automatic shooting
        //Left click hold to shoot
        if (Input.GetButton("Fire1") && CurrentAmmo != 0 && !_isReloading)
        {
            Shoot();
        }

        //Aiming
        //Toggle camera FOV when right click is held down
        if (Input.GetButton("Fire2") && !_isReloading)
        {
            _isAiming = true;
        }
        else
        {
            _isAiming = false;
        }

        //Press R to reload 
        if (Input.GetKeyDown(KeyCode.R) && !_isReloading)
        {
            //Play reload animation 
            _anim.Play("Reload", 0, 0f);
            //Delay reload process
            StartCoroutine(Reload());
            //Play reload sound
            _mainAudioSource.clip = _reloadSound;
            _mainAudioSource.Play();
        }
    }

    //AUtomatic fire
    private void Shoot()
    {
        if (Time.time - _lastFired > 1 / _fireRate)
        {
            _lastFired = Time.time;
            //Remove 1 bullet from ammo
            CurrentAmmo -= 1;

            //Shoot once, muzzleFlash once 
            _muzzleFlash.Emit(1);

            //Shooting sound play
            _shootAudioSource.Play();

            if (!_isAiming)
            {
                //Play shooting animation
                _anim.Play("Fire", 0, 0f);
            }
            if (_isAiming)
            {
                _anim.Play("Aim Fire", 0, 0f);
            }

            //Instantiate bullet from bulletSpawnPoint
            GameObject bullet = Instantiate(_bulletPrefab, _bulletSpawnPoint.transform.position, _bulletSpawnPoint.transform.rotation);
            GameObject casing = Instantiate(_casingPrefab, _casingSpawnPoint.transform.position, _bulletSpawnPoint.transform.rotation);

            //Add velocity to the bullet 
            bullet.GetComponent<Rigidbody>().velocity = bullet.transform.forward * _bulletForce;
            casing.GetComponent<Rigidbody>().velocity = casing.transform.right*_casingForce;

        }
    }
    private void Aiming()
    {
        //Change  GunCamera FOV when right click is held down
        if (_isAiming)
        {
            //Play aiming animation
            _anim.SetBool("Aim", true);
            //Change Fov to Scope Fov
            _gunCamera.fieldOfView = Mathf.Lerp(_gunCamera.fieldOfView, _scopeFov, _openScopeSpeed * Time.deltaTime);
        }
        else
        {
            //Stop aiming animation
            _anim.SetBool("Aim", false);
            //Change Fov to Default Fov
            _gunCamera.fieldOfView = Mathf.Lerp(_gunCamera.fieldOfView, _defaultFov, _openScopeSpeed * Time.deltaTime);
        }

    }


    private IEnumerator Reload()
    {
        //Delay corrent Ammo reload  
        yield return new WaitForSeconds(_reloadDelay);
        //Full of Ammo
        CurrentAmmo = Ammo;

    }

    private void AnimationCheck()
    {
        //Check if reloading
        if (_anim.GetCurrentAnimatorStateInfo(0).IsName("Reload"))
        {
            _isReloading = true;
        }
        else
        {
            _isReloading = false;
        }
    }

}
