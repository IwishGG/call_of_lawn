﻿using System;
using UnityEngine;

public class CamerController : MonoBehaviour
{
    //Get help from online
    [SerializeField]
    Transform Player,PlayerArms = null;

    [SerializeField]
    private float _mouseSensitivity = 100f;
    private float MouseX;
    private float MouseY;

    // Start is called before the first frame update
    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
    }

    // Update is called once per frame
    void Update()
    {
        CameraRotate();
    }

    private void CameraRotate()
    {
        MouseX = Input.GetAxis("Mouse X");
        MouseY = Input.GetAxis("Mouse Y");

        float rotateAmountX = MouseX * _mouseSensitivity * Time.deltaTime;
        float rotateAmountY = MouseY * _mouseSensitivity * Time.deltaTime;

        Vector3 rotatePlayer = Player.transform.rotation.eulerAngles;
        Vector3 rotatePlayerArms = PlayerArms.transform.rotation.eulerAngles;

        //Arms control horizontal rotation
        rotatePlayerArms.x -= rotateAmountY;
        rotatePlayerArms.z = 0f;
        //Player control vertical rotation
        rotatePlayer.y += rotateAmountX;
        rotatePlayerArms.y = rotatePlayer.y;

        Player.rotation = Quaternion.Euler(rotatePlayer);
        PlayerArms.rotation = Quaternion.Euler(rotatePlayerArms);

    }
}
