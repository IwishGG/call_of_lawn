﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    [SerializeField]
    private GameObject _uiGameWin;
    [SerializeField]
    private GameObject _uiGameLose;
    [SerializeField]
    private int _winScore = 5000;
    public static int Winscore;
    public static int Score;
    public static float BaseHealth;
    // Start is called before the first frame update
    void Start()
    {
        Winscore = _winScore;
        Time.timeScale = 1f;
    }
    private void Update()
    {
        GameCheck();
    }

    private void GameCheck()
    {
        if (GameManager.Score >= Winscore)
        {

            // Ddelay  to do game win 
            Invoke("Winning", 0.5f);
            return;
        }

        if (GameManager.Score <= Winscore && GameManager.BaseHealth <= 0)
        {

            //Ddelay to do game lose 
            Invoke("Losing", 0.5f);
            return;
        }
    }

    private void Winning()
    {
        //pause game 
        Time.timeScale = 0f;
        //show cursor
        Cursor.lockState = CursorLockMode.None;
        // show game win ui 
        _uiGameWin.SetActive(true);

    }
    private void Losing()
    {
        //pause game 
        Time.timeScale = 0f;
        //show cursor
        Cursor.lockState = CursorLockMode.None;
        // show game win ui 
        _uiGameLose.SetActive(true);

    }
}
