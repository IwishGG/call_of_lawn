﻿using UnityEngine;

public class EnemyState : MonoBehaviour
{
    //Enemy Health Point
    [SerializeField]
    private int _health = 100;
    //Defeat Each enemy Score
    [SerializeField]
    private int _enemyScore = 100;
    //dead ImpactPrefabs
    [SerializeField]
    private Transform _deadImpactPrefabs;
    //Recive demage  
    private int _receivedDamage;


    private void Awake()
    {

    }
    void Start()
    {
        //Recive demage from Weapon
        _receivedDamage = Weapon_Ak.Damage;
    }

    private void OnCollisionEnter(Collision collision)
    {
        //Check if collision to bullet
        if (collision.transform.tag == "Bullet")
        {
            TakeDamage();
        }
    }
    private void TakeDamage()
    {
        //Reduce Health point 
        _health -= _receivedDamage;
        //Check if die
        if (_health <= 0f)
        {
            Die();
        }
    }
    private void Die()
    {
        //Total score plus this enemy score
        GameManager.Score += _enemyScore;
        //instantiate a dead Impact
        Instantiate(_deadImpactPrefabs, transform.position, transform.rotation);
        //Destroy 
        Destroy(gameObject);
    }



}
