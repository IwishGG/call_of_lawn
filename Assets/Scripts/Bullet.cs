﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    // After how long time should the bullet prefab be destroyed
    [SerializeField]
    private float _bulletLifeSpawn = 5f;
    
    [Header("ImpactPrefabs")]
    [SerializeField]
    private Transform _bloodImpactPrefabs;

    [SerializeField]
    private Transform _defaultImpactPrefabs;
    private bool _destroyOnImpact;

    private void Awake()
    {
        _destroyOnImpact = false;

        //If destroy on impact is false, destroy bullet after few time 
        if (_destroyOnImpact == false)
        {

            Destroy(gameObject, _bulletLifeSpawn);
        }
        //Otherwise, destroy bullet on impact
        else
        {
            Destroy(gameObject);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {


        //If bullet collides with "Blood" tag
        if (collision.transform.tag == "Blood")
        {
            //Instantiate random impact prefab from array
            Instantiate(_bloodImpactPrefabs, transform.position, Quaternion.LookRotation(collision.contacts[0].normal));
            //Destroy bullet object
            Destroy(gameObject);
        }

        //If bullet collides with "Default" tag
        if (collision.transform.tag == "Default")
        {
            //Instantiate random impact prefab from array
            Instantiate(_defaultImpactPrefabs, transform.position, Quaternion.LookRotation(collision.contacts[0].normal));
            //Destroy bullet object
            Destroy(gameObject);
        }

    }
}

